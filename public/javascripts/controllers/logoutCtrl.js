/**
 * Created by dinukshai on 12/2/2016.
 */
app.controller('logoutCtrl', [
    'backEndCommunicatorService',
    '$scope',
    '$route',
    function ($scope,
              $route,
              backEndCommunicatorService
            )  {
            $scope.logout = function () {
            {
                var config = {
                    headers: {
                        'X-HTTP-Method-Override': 'GET'
                    }
                };
                var url = routeStrService.serverBaseURI()+routeStrService.getPathStr().logout_path;
                sessionService.destroy('ukey');
                var respond = $http.get(url, config)
                $window.location.href = routeStrService.serverBaseURI()+"admin";

            }
        };
    }
]);