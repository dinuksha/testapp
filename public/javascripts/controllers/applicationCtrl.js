/**
 * Created by dinukshai on 12/8/2016.
 */
app.controller('applicationCtrl', [
    '$scope',
    '$route',
    'backEndCommunicatorService',
    function ($scope,
              $route,
              backEndCommunicatorService) {
            backEndCommunicatorService.getRq("/user/info").then(function (d) {
                $scope.appL = d['data'];
            });
    }
]);