/**
 * Created by dinukshai on 12/2/2016.
 */
app.factory('backEndCommunicatorService',function($http,$location,routeStrService){
    var baseUrl= routeStrService.serverBaseURI();
    var backEndCommunicatorService={
        getRq: function(route) {
            // $http returns a promise, which has a then function, which also returns a promise
            var url = baseUrl+route;
            var promise = $http.get(url).then(function (response) {
                // The then function here is an opportunity to modify the response

                // The return value gets picked up by the then in the controller.
                return response.data;
            });
            // Return the promise to the controller
            return promise;
        },
        sendRq:function(data,route){
            var config = {
                headers: {
                    'X-HTTP-Method-Override': 'POST',
                    'Authorization':'JWThere'
                }
            };
            var url = baseUrl+route;
            var respond =  $http.post(url, data,config).then(function(response){
                return response.data;
            });
            return respond;
        },
        deleteRq:function(identifier,route){
            var url = baseUrl+route+identifier;
            var respond =  $http.delete(url).then(function(response){
                return response.data;
            });
            return respond;
        },
        updateRq:function(data,route){
            var config = {
                headers: {
                    'X-HTTP-Method-Override': 'PUT',
                    'Authorization':'JWThere'
                }
            };
            var url = baseUrl+route;
            var respond =  $http.put(url,data,config).then(function(response){
                return response.data;
            });
            return respond;
        }
    };
    return backEndCommunicatorService;
});