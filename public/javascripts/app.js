'use strict';

var app = angular.module('KouimTestApp',['ngRoute','ngNotify']);

app.config(['$routeProvider',function($routeProvider){
    $routeProvider
        .when('/',{templateUrl:'assets/src/application.html'})
        .when('/users',{templateUrl:'assets/src/users.html'})
        .when('/roles',{templateUrl:'assets/src/roles.html'})
        .otherwise({redirectTo:'/'});
}]);

app.run([
    'ngNotify',
    function(ngNotify) {
        ngNotify.config({
            theme: 'pure',
            position: 'bottom'
        });
    }
]);