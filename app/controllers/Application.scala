package controllers

import java.util.UUID

import com.nimbusds.jose.PlainObject
import play.api._
import play.api.libs.json.{Json, JsValue}
import play.api.mvc._
import util.OAuth2
import util.OAuth2._

object Application extends Controller {

  def index = Action { implicit request =>
    val oauth2 = new OAuth2(Play.current)
    if(request.session.get("oauth-token").isEmpty) {
      //    val callbackUrl = util.routes.OAuth2.callback(None,None).absoluteURL()
      val scope = "openid" // github scope - request repo access
      val state = UUID.randomUUID().toString // random confirmation string
      val redirectUrl = oauth2.getAuthorizationUrl("code", "http://10.101.1.210:8000/_oauth-callback", scope, state)
      Ok(views.html.index("Your new application is ready.", redirectUrl)).
        withSession("oauth-state" -> state)
    }
    else {
      val x = request.session.get("oauth-token")
      if(show(x).equals("?")){
        BadRequest("Not Authorized")
      }

      val json: JsValue = Json.parse(show(x))
      val id : String = json.\("id_token").as[String]
      val plainObject = PlainObject.parse(id);
      Ok(views.html.starter("Welcome " + plainObject.getPayload.toJSONObject.get("sub")))
    }
  }

}
