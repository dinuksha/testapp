//package util
//
//import play.Application
//import play.api.Play
//import play.api.http.HeaderNames
//import play.api.libs.json.{Json, JsValue}
//import play.api.libs.ws.WS
//import play.api.mvc.{Controller, Action}
//
//import scala.concurrent.Future
//
///**
// * Created by dinukshai on 12/2/2016.
// */
//class AppInfo(application: Application)  {
//
//}
//
//object AppInfo extends Controller {
//
//  def getApplicationList() = Action.async { request =>
//    implicit val app = Play.current
//    request.session.get("oauth-token").fold(Future.successful(Unauthorized("Access Restricted"))) { authToken =>
//      WS.url("http://10.101.3.23:9000/op/resource/applications").
//        withHeaders(HeaderNames.AUTHORIZATION -> s"Bearer $authToken").
//        get().map { response =>
//        Ok(response.json)
//      }
//    }
//  }
//
//  def getApplicationById(id: String) = Action.async { request =>
//    implicit val app = Play.current
//    request.session.get("oauth-token").fold(Future.successful(Unauthorized("Access Restricted"))) { authToken =>
//      WS.url("http://10.101.3.23:9000/op/resource/application/"+id).
//        withHeaders(HeaderNames.AUTHORIZATION -> s"Bearer $authToken").
//        get().map { response =>
//        Ok(response.json)
//      }
//    }
//  }
//
//  def userInfo() = Action.async { request =>
//    implicit val app = Play.current
//    request.session.get("oauth-token").fold(Future.successful(Unauthorized("Access Restricted"))) { authToken =>
//      WS.url("http://10.101.3.23:9000/op/resource/userInfo").
//        withHeaders(HeaderNames.AUTHORIZATION -> s"Bearer $authToken").
//        get().map { response =>
//        Ok(response.json)
//      }
//    }
//  }
//
//  def show(x: Option[String]) = x match {
//    case Some(s) => s
//    case None => "?"
//  }
//}
