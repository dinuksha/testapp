package util

import com.nimbusds.jose.PlainObject
import play.Logger
import play.api.Application
import play.api.Play
import play.api.http.{MimeTypes, HeaderNames}
import play.api.libs.ws.WS
import play.api.mvc.{Session, Action, Controller}
import play.api.libs.json.{JsValue, Json}
import scala.concurrent.{Future}
import scala.concurrent.ExecutionContext.Implicits.global


class OAuth2(application: Application) {
  lazy val githubAuthId = application.configuration.getString("github.client.id").get

  def getAuthorizationUrl(response_type: String, redirectUri: String, scope: String, state: String): String = {
    val baseUrl = application.configuration.getString("github.redirect.url").get
    baseUrl.format(response_type, githubAuthId, redirectUri, scope, state)
  }

  def getToken(code: String): Future[String] = {
    val tokenResponse = WS.url("http://10.101.3.23:9000/oauthp/token")(application).
      withHeaders(HeaderNames.ACCEPT -> MimeTypes.JSON).
      post(Map("client_id" -> Seq("testApp"),"grant_type" -> Seq("authorization_code"),"code" -> Seq(code), "redirect_uri" -> Seq("http://10.101.1.210:8000/test")))

    tokenResponse.flatMap { response =>
      Future.successful(response.json.toString())
    }
  }

  def getRefreshToken(refreshToken: String): Future[String] = {
    val tokenResponse = WS.url("http://10.101.3.23:9000/oauthp/token")(application).
      withHeaders(HeaderNames.ACCEPT -> MimeTypes.JSON).
      post(Map("client_id" -> Seq("testApp"), "grant_type" -> Seq("refresh_token"), "refresh_token" -> Seq(refreshToken),"scope" -> Seq("openid")))

    tokenResponse.flatMap { response =>
      Future.successful(response.json.toString())
    }
  }

  def getUserInfo(accessToken: String): Future[String] = {
    val tokenResponse = WS.url("http://10.101.3.23:9000/op/resource/userInfo")(application).
      withHeaders(HeaderNames.AUTHORIZATION -> s"Bearer $accessToken").
      get()

    tokenResponse.flatMap { response =>
      Future.successful(response.json.toString())
    }
  }
  def getAppInfo(accessToken: String): Future[String] = {
    val tokenResponse = WS.url("http://10.101.3.23:9000/op/resource/applications")(application).
      withHeaders(HeaderNames.AUTHORIZATION -> s"Bearer $accessToken").
      get()

    tokenResponse.flatMap { response =>
      Future.successful(response.json.toString())
    }
  }

}

object OAuth2 extends Controller {
  lazy val oauth2 = new OAuth2(Play.current)

  def callback(codeOpt: Option[String] = None) = Action.async { implicit request =>
    (for {
      code <- codeOpt
    } yield {
        oauth2.getToken(code).map { accessToken =>
        Redirect(util.routes.OAuth2.success()).withSession("oauth-token" -> accessToken)
        }.recover {
          case ex: IllegalStateException => Unauthorized(ex.getMessage)
        }
    }).getOrElse(Future.successful(BadRequest("No parameters supplied")))
  }

  def success = Action { request =>
    implicit val app = Play.current
    val x = request.session.get("oauth-token")
    if(show(x).equals("?")){
      BadRequest("Not Authorized")
    }

    val json: JsValue = Json.parse(show(x))
    val id : String = json.\("id_token").as[String]
    val plainObject = PlainObject.parse(id);
    Ok(views.html.starter("Welcome " + plainObject.getPayload.toJSONObject.get("sub")))
  }


  def refresh() = Action.async { implicit request =>
    val json: JsValue = Json.parse(show(request.session.get("oauth-token")))
    val id : String = json.\("refresh_token").as[String]
    oauth2.getRefreshToken(id).map { accessToken =>
      play.Logger.debug("hellllllllo "+accessToken);
      Redirect(util.routes.OAuth2.test()).withSession("refresh-token" -> accessToken)
    }
  }

  def test() = Action { request =>
    implicit val app = Play.current
    val x = request.session.get("refresh-token")
    Ok("Refresh Token:" + x)
  }

  def show(x: Option[String]) = x match {
    case Some(s) => s
    case None => "?"
  }

  def userInfo() = Action.async { request =>
    val json: JsValue = Json.parse(show(request.session.get("oauth-token")))
    val id : String = json.\("access_token").as[String]
    oauth2.getUserInfo(id).map { response =>
      Ok(response)
    }
  }

  def appInfo() = Action.async { request =>
    val json: JsValue = Json.parse(show(request.session.get("oauth-token")))
    val id : String = json.\("access_token").as[String]
    oauth2.getAppInfo(id).map { response =>
      Ok(response)
    }
  }
//  def getApplicationList() = Action.async { request =>
//    implicit val app = Play.current
//    request.session.get("oauth-token").fold(Future.successful(Unauthorized("No way Jose"))) { authToken =>
//      WS.url("http://localhost:9000/admin").
//        withHeaders(HeaderNames.AUTHORIZATION -> s"token $authToken").
//        get().map { response =>
//          Ok(response.json)
//        }
//    }
//  }
}